import argparse
import json
import os
from pprint import pprint
import time


from worker import meta
from worker.storage import GoogleStorage


def core(bangu_name, video_id, is_playlist=True):
    '''
    {
        'id': 'av21670058',
        'playlist': [{
            'ep': 1,
            'titile': ''
        }...],
    }

    '''
    tmp_folder_path = 'tmp/{}'.format(bangu_name)
    os.makedirs(tmp_folder_path, exist_ok=True)
    titles = meta.extract_meta_title(video_id, is_playlist)
    print('Titles: {}'.format(titles))
    cmds = ['annie', '-p', '-f default', '-o', tmp_folder_path, video_id]
    if is_playlist is False:
        cmds.remove('-p')
    os.system(' '.join(cmds))
    create_metafile(tmp_folder_path, titles)
    storage = GoogleStorage()
    for file in os.listdir(tmp_folder_path):
        if 'METADATA' == file:
            storage.upload_blob(
                bucket_name='nogi-bangu',
                source_file_name='{}/{}'.format(tmp_folder_path, file),
                destination_blob_name='{}/METADATA'.format(bangu_name)
            )
        else:
            storage.upload_blob(
                bucket_name='nogi-bangu',
                source_file_name='{}/{}'.format(tmp_folder_path, file),
                destination_blob_name='{}/ep{}'.format(
                    bangu_name,
                    str(titles.index(file[:-4]) + 1).zfill(2))
            )


def create_metafile(tmp_folder_path, titles):
    with open('{}/METADATA'.format(tmp_folder_path), 'w') as writer:
        for title in titles:
            writer.write(title)
            writer.write('\n')


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-b', '--bangu', required=True)
    parser.add_argument('-v', '--video-id', required=True)
    parser.add_argument('-p', '--is-playlist', action='store_true')
    args = parser.parse_args()
    core(bangu_name=args.bangu, video_id=args.video_id, is_playlist=args.is_playlist)
