import json
import os
import editdistance


def extract_meta_title(video_id, is_playlist=True):
    cmds = ['annie', '-p', '-j', video_id, '>', '{}.meta'.format(video_id)]
    if is_playlist is False:
        cmds.remove('-p')
    print(' '.join(cmds))
    os.system(' '.join(cmds))
    with open('{}.meta'.format(video_id), 'r') as reader:
        lines = reader.readlines()
        new_line = '[' + ''.join(lines).replace('}\n{', '},{') + ']'
    reader.close()
    return [meta['Title'] for meta in json.loads(new_line)]


def most_similar_title(src_title, meta_titles):
    similarity = 1000
    result = None
    for title in meta_titles:
        diff = abs(editdistance.eval(src_title, title))
        if diff < similarity:
            similarity = diff
            result = title
    return result
